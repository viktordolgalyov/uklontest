package com.uklontest.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(entities = arrayOf(LocationAddress::class), version = 3, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        const val NAME = "uklon_test.db"
    }

    abstract fun addressDao(): AddressDao
}