package com.uklontest.data

import android.location.Geocoder
import android.text.TextUtils
import com.google.android.gms.maps.model.PolylineOptions
import com.google.maps.DirectionsApi
import com.google.maps.GeoApiContext
import com.google.maps.PendingResult
import com.google.maps.android.PolyUtil
import com.google.maps.model.DirectionsResult
import com.google.maps.model.LatLng
import com.google.maps.model.Unit

class Repository(private val geoContext: GeoApiContext,
                 private val geocoder: Geocoder,
                 private val database: AppDatabase) {

    fun getAddress(lat: Double, lng: Double, listener: Listener<String>) {
        Thread({
            val addresses = geocoder.getFromLocation(lat, lng, 1)
            val address = addresses?.firstOrNull()
            address?.let {
                val addressLine = address.getAddressLine(0)
                val city = address.locality
                val state = address.adminArea
                val country = address.countryName
                listener.onSuccess(TextUtils.join(", ", arrayOf(addressLine, city, state, country)))
            }
        }).start()
    }

    fun getRoute(originLat: Double, originLng: Double, destLat: Double, destLng: Double, listener: Listener<PolylineOptions>) {
        val request = DirectionsApi.newRequest(geoContext)
                .origin(LatLng(originLat, originLng))
                .destination(LatLng(destLat, destLng))
                .units(Unit.METRIC)
        request.setCallback(object : PendingResult.Callback<DirectionsResult> {
            override fun onResult(result: DirectionsResult?) {
                result?.let {
                    val opt = PolylineOptions()
                    for (r in result.routes) {
                        opt.addAll(PolyUtil.decode(r.overviewPolyline.encodedPath))
                    }
                    listener.onSuccess(opt)
                }
            }

            override fun onFailure(e: Throwable?) {
                listener.onError(e)
            }
        })
    }

    fun putAddress(address: LocationAddress) {
        Thread({
            database.addressDao().put(address)
        }).start()
    }

    fun getLastAddresses(count: Int, listener: Listener<List<LocationAddress>>) {
        Thread({
            val result = database.addressDao().getLast(count)
            listener.onSuccess(result)
        }).start()
    }
}