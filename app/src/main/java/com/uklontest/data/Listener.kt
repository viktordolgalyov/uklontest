package com.uklontest.data

interface Listener<in T> {
    fun onSuccess(data: T)
    fun onError(error: Throwable?) {

    }
}