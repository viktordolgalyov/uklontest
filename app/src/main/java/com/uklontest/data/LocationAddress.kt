package com.uklontest.data

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

@Entity(tableName = "addresses")
class LocationAddress(
        @PrimaryKey(autoGenerate = true) val id: Int? = null,
        val lat: Double,
        val lng: Double,
        val description: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readDouble(),
            parcel.readDouble(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeDouble(lat)
        parcel.writeDouble(lng)
        parcel.writeString(description)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LocationAddress> {
        override fun createFromParcel(parcel: Parcel): LocationAddress {
            return LocationAddress(parcel)
        }

        override fun newArray(size: Int): Array<LocationAddress?> {
            return arrayOfNulls(size)
        }
    }
}