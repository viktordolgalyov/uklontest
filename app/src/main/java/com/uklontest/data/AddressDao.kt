package com.uklontest.data

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

@Dao
interface AddressDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun put(address: LocationAddress)

    @Query("SELECT * FROM addresses ORDER BY id DESC LIMIT :count")
    fun getLast(count: Int): List<LocationAddress>
}