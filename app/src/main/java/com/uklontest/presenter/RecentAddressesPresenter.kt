package com.uklontest.presenter

import com.uklontest.data.Listener
import com.uklontest.data.LocationAddress
import com.uklontest.data.Repository
import com.uklontest.view.recent.RecentAddressesView

class RecentAddressesPresenter(private val repository: Repository) {
    private var view: RecentAddressesView? = null
    private var data: List<LocationAddress>? = null

    fun bindView(view: RecentAddressesView) {
        this.view = view
        loadData()
    }

    fun unbindView() {
        this.view = null
    }

    fun onItemClick(index: Int) {
        data?.get(index)?.let { view?.deliverResult(it) }
    }

    fun loadData() {
        repository.getLastAddresses(10, object : Listener<List<LocationAddress>> {
            override fun onSuccess(data: List<LocationAddress>) {
                this@RecentAddressesPresenter.data = data
                view?.setData(data)
            }
        })
    }
}