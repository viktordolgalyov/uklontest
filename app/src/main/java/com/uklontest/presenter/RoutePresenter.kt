package com.uklontest.presenter

import android.support.annotation.IntDef
import com.google.android.gms.maps.model.*
import com.uklontest.data.Listener
import com.uklontest.data.LocationAddress
import com.uklontest.data.Repository
import com.uklontest.view.route.RouteView

class RoutePresenter(private val repository: Repository) {
    private var view: RouteView? = null
    private var origin: Marker? = null
    private var destination: Marker? = null
    private var route: Polyline? = null
    private var pendingOrigin: LocationAddress? = null
    private var pendingDestination: LocationAddress? = null

    fun bindView(view: RouteView) {
        this.view = view
        pendingOrigin?.let {
            view.setOrigin(MarkerOptions().position(LatLng(it.lat, it.lng)), it.description)
            pendingOrigin = null
        }
        pendingDestination?.let {
            view.setDestination(MarkerOptions().position(LatLng(it.lat, it.lng)), it.description)
            pendingDestination = null
        }
    }

    fun unbindView() {
        this.view = null
    }

    fun onMapClick(point: LatLng, @Mode mode: Long) {
        route?.remove()
        loadAddress(point, object : Listener<String> {
            override fun onSuccess(data: String) {
                storeAddress(point, data) // maybe store only after the start of a route
                val opt = MarkerOptions().position(point)
                when (mode) {
                    MODE_ORIGIN -> view?.setOrigin(opt, data)
                    MODE_DESTINATION -> view?.setDestination(opt, data)
                }
            }
        })
    }

    fun onPickOriginClick() {
        view?.openOriginPicker()
    }

    fun onPickDestinationClick() {
        view?.openDestinationPicker()
    }

    fun onStartRouteClick() {
        if (origin != null && destination != null) {
            loadRoute()
        }
    }

    fun onOriginAvailable(marker: Marker) {
        origin?.remove()
        origin = marker
    }

    fun onDestinationAvailable(marker: Marker) {
        destination?.remove()
        destination = marker
    }

    fun onRouteAvailable(route: Polyline) {
        this.route = route
    }

    fun onOriginPickSuccess(address: LocationAddress?) {
        address?.let {
            val opt = MarkerOptions().position(LatLng(it.lat, it.lng))
            when (view) {
                null -> pendingOrigin = it
                else -> view!!.setOrigin(opt, it.description)
            }
        }
    }

    fun onDestinationPickSuccess(address: LocationAddress?) {
        address?.let {
            val opt = MarkerOptions().position(LatLng(it.lat, it.lng))
            when (view) {
                null -> pendingDestination = it
                else -> view!!.setDestination(opt, it.description)
            }
        }
    }

    private fun loadAddress(coordinates: LatLng, listener: Listener<String>) {
        repository.getAddress(coordinates.latitude, coordinates.longitude, listener)
    }

    private fun loadRoute() {
        repository.getRoute(origin!!.position.latitude,
                origin!!.position.longitude,
                destination!!.position.latitude,
                destination!!.position.longitude,
                object : Listener<PolylineOptions> {
                    override fun onSuccess(data: PolylineOptions) {
                        view?.setRoute(data)
                    }

                    override fun onError(error: Throwable?) {
                        super.onError(error)
                        view?.clear()
                    }
                })
    }

    private fun storeAddress(point: LatLng, description: String) {
        val locationAddress = LocationAddress(lat = point.latitude, lng = point.longitude, description = description)
        repository.putAddress(locationAddress)
    }

    companion object {
        const val MODE_ORIGIN = 0L
        const val MODE_DESTINATION = 1L

        @IntDef(MODE_ORIGIN, MODE_DESTINATION)
        @Retention(AnnotationRetention.SOURCE)
        annotation class Mode
    }

}