package com.uklontest.presenter

import android.arch.persistence.room.Room
import android.content.Context
import android.location.Geocoder
import com.google.maps.GeoApiContext
import com.uklontest.data.AppDatabase
import com.uklontest.data.Repository
import com.uklontest.view.recent.RecentAddressesFragment
import com.uklontest.view.route.RouteFragment
import java.util.*

object PresenterInjector {

    fun inject(target: RouteFragment) {
        target.presenter = RoutePresenter(
                Repository(provideGeoContext(),
                        provideGeoCoder(target.context),
                        provideDatabase(target.context))
        )
    }

    fun inject(target: RecentAddressesFragment) {
        target.presenter = RecentAddressesPresenter(
                Repository(provideGeoContext(),
                        provideGeoCoder(target.context),
                        provideDatabase(target.context))
        )
    }

    /*
    In the real world we can't store the api key as is in the source code, but because it's just sample we can
     */
    private fun provideApiKey(): String {
        return "AIzaSyAwhXyjivFKLk1mm-dnwaGl7s4UTjXQ4k8"
    }

    private fun provideGeoContext(): GeoApiContext {
        return GeoApiContext.Builder().apiKey(provideApiKey()).build()
    }

    private fun provideGeoCoder(context: Context): Geocoder {
        return Geocoder(context.applicationContext, Locale.getDefault())
    }

    private fun provideDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, AppDatabase.NAME)
                .fallbackToDestructiveMigration()
                .build()
    }
}