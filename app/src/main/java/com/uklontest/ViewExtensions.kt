package com.uklontest

import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun ViewGroup.inflateChild(@LayoutRes layoutRes: Int): View? {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}