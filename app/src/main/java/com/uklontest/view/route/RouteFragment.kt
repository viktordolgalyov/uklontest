package com.uklontest.view.route

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.ActivityCompat.checkSelfPermission
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import com.uklontest.R
import com.uklontest.data.LocationAddress
import com.uklontest.inflateChild
import com.uklontest.presenter.PresenterInjector
import com.uklontest.presenter.RoutePresenter
import com.uklontest.view.recent.RecentAddressesActivity
import com.uklontest.view.recent.RecentAddressesFragment
import kotlinx.android.synthetic.main.fragment_map.*

class RouteFragment : Fragment(), OnMapReadyCallback, RouteView {
    lateinit var presenter: RoutePresenter
    private var map: GoogleMap? = null


    companion object {
        val REQUEST_CODE_PERMISSIONS = 1
        fun create(): Fragment {
            return RouteFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        PresenterInjector.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflateChild(R.layout.fragment_map)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startRoute.requestFocus()
        startRoute.setOnClickListener({ presenter.onStartRouteClick() })
        pickOrigin.setOnClickListener { presenter.onPickOriginClick() }
        pickDestination.setOnClickListener { presenter.onPickDestinationClick() }
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)
    }

    override fun onStart() {
        super.onStart()
        presenter.bindView(this)
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSIONS && permissions.isNotEmpty()) {
            if (grantResults.first() == PackageManager.PERMISSION_GRANTED) {
                initMap()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val addressTag = RecentAddressesFragment.EXTRA_ADDRESS
        data?.let {
            if (resultCode == Activity.RESULT_OK && it.hasExtra(addressTag)) {
                val address = it.getParcelableExtra<LocationAddress>(addressTag)
                when (requestCode) {
                    RecentAddressesActivity.REQUEST_CODE_ORIGIN -> {
                        presenter.onOriginPickSuccess(address)
                    }
                    RecentAddressesActivity.REQUEST_CODE_DESTINATION -> {
                        presenter.onDestinationPickSuccess(address)
                    }
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        presenter.unbindView()
        mapView.onStop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onMapReady(map: GoogleMap?) {
        this.map = map
        map?.setOnMapClickListener { it?.let { onMapClick(it) } }
        when {
            !hasLocationPermission() -> requestPermissionsInternal()
            else -> initMap()
        }
    }

    override fun setOrigin(position: MarkerOptions, address: String) {
        mapView.post({
            val marker = map?.addMarker(position)
            marker?.let {
                origin.setText(address)
                presenter.onOriginAvailable(it)
            }
        })
    }

    override fun setDestination(position: MarkerOptions, address: String) {
        mapView.post({
            val marker = map?.addMarker(position)
            marker?.let {
                destination.setText(address)
                presenter.onDestinationAvailable(it)
            }
        })
    }

    override fun setRoute(route: PolylineOptions) {
        mapView.post({
            route.color(Color.BLUE)
            val polyline = map?.addPolyline(route)
            polyline?.let {
                moveCameraToPolyline(it)
                presenter.onRouteAvailable(it)

            }
        })
    }

    override fun clear() {
        map?.clear()
        origin.text = null
        destination.text = null
    }

    override fun openOriginPicker() {
        RecentAddressesActivity.launch(RecentAddressesActivity.REQUEST_CODE_ORIGIN, this)
    }

    override fun openDestinationPicker() {
        RecentAddressesActivity.launch(RecentAddressesActivity.REQUEST_CODE_DESTINATION, this)
    }

    @SuppressWarnings("MissingPermission")
    private fun initMap() {
        map?.isMyLocationEnabled = hasLocationPermission()
        map?.uiSettings?.isMyLocationButtonEnabled = hasLocationPermission()
    }

    private fun moveCameraToPolyline(polyline: Polyline) {
        val boundsBuilder = LatLngBounds.builder()
        for (point in polyline.points) {
            boundsBuilder.include(point)
        }
        val padding = resources.getDimensionPixelSize(R.dimen.padding)
        map?.animateCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), padding))
    }

    private fun onMapClick(coord: LatLng) {
        if (origin.isFocused) {
            presenter.onMapClick(coord, RoutePresenter.MODE_ORIGIN)
        } else if (destination.isFocused) {
            presenter.onMapClick(coord, RoutePresenter.MODE_DESTINATION)
        }
    }

    private fun requestPermissionsInternal() {
        val permissions = Array(1, { Manifest.permission.ACCESS_FINE_LOCATION })
        requestPermissions(permissions, REQUEST_CODE_PERMISSIONS)
    }

    private fun hasLocationPermission(): Boolean {
        return checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }
}