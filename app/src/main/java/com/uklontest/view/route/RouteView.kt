package com.uklontest.view.route

import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions

interface RouteView {
    fun setOrigin(position: MarkerOptions, address: String)
    fun setDestination(position: MarkerOptions, address: String)
    fun setRoute(route: PolylineOptions)
    fun clear()
    fun openOriginPicker()
    fun openDestinationPicker()
}