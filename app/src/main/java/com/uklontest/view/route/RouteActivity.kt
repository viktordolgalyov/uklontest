package com.uklontest.view.route

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class RouteActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(android.R.id.content, RouteFragment.create())
                    .commitNow()
        }
    }
}