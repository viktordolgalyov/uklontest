package com.uklontest.view

@FunctionalInterface
interface OnItemClickListener {

    fun onItemClick(position: Int)
}