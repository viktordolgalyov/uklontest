package com.uklontest.view.recent

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.uklontest.R
import com.uklontest.data.LocationAddress
import com.uklontest.inflateChild
import com.uklontest.presenter.PresenterInjector
import com.uklontest.presenter.RecentAddressesPresenter
import kotlinx.android.synthetic.main.fragment_recent_addresses.*

class RecentAddressesFragment : Fragment(), RecentAddressesView {
    lateinit var presenter: RecentAddressesPresenter

    companion object {
        const val EXTRA_ADDRESS = "com.uklontest.view.recent.RecentAddressesFragment_EXTRA_ADDRESS"
        fun create(): Fragment {
            return RecentAddressesFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        PresenterInjector.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflateChild(R.layout.fragment_recent_addresses)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recent.layoutManager = LinearLayoutManager(context)
        recent.adapter = AddressAdapter({ presenter.onItemClick(it) })
    }

    override fun onStart() {
        super.onStart()
        presenter.bindView(this)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            activity.finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStop() {
        super.onStop()
        presenter.unbindView()
    }

    override fun setData(data: List<LocationAddress>) {
        recent.post({
            (recent.adapter as? AddressAdapter)?.addData(data)
        })
    }

    override fun deliverResult(address: LocationAddress) {
        val intent = Intent()
        intent.putExtra(EXTRA_ADDRESS, address)
        activity.setResult(Activity.RESULT_OK, intent)
        activity.finish()
    }
}