package com.uklontest.view.recent

import com.uklontest.data.LocationAddress

interface RecentAddressesView {
    fun setData(data: List<LocationAddress>)
    fun deliverResult(address: LocationAddress)
}