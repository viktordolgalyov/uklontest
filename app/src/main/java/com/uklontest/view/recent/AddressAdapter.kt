package com.uklontest.view.recent

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.uklontest.R
import com.uklontest.data.LocationAddress
import com.uklontest.inflateChild

class AddressAdapter(private val listener: (Int) -> Unit) : RecyclerView.Adapter<AddressAdapter.AddressHolder>() {
    private val items: MutableList<LocationAddress> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressHolder {
        val itemView = parent.inflateChild(R.layout.item_address)!!
        return AddressHolder(itemView, listener)
    }

    override fun onBindViewHolder(holder: AddressHolder?, position: Int) {
        val address = items[position]
        holder?.text?.text = address.description
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class AddressHolder(itemView: View, listener: (Int) -> Unit) : RecyclerView.ViewHolder(itemView) {
        internal val text: TextView = itemView.findViewById(R.id.text)

        init {
            itemView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    listener(adapterPosition)
                }
            }
        }
    }

    fun addData(data: List<LocationAddress>) {
        val prevSize = itemCount
        items.addAll(itemCount, data)
        notifyItemRangeInserted(prevSize, data.size)
    }
}