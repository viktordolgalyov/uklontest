package com.uklontest.view.recent

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity

class RecentAddressesActivity : AppCompatActivity() {

    companion object {
        const val REQUEST_CODE_ORIGIN = 1
        const val REQUEST_CODE_DESTINATION = 2

        fun launch(requestCode: Int, target: Fragment) {
            val launch = Intent(target.context, RecentAddressesActivity::class.java)
            target.startActivityForResult(launch, requestCode)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(android.R.id.content, RecentAddressesFragment.create(), RecentAddressesFragment::class.java.simpleName)
                    .commitNow()

        }
    }
}